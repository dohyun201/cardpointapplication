package com.example.jupjupapplication.fragment

import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import com.example.jupjupapplication.R
import com.example.jupjupapplication.Util.SharedUtil
import com.example.jupjupapplication.retrofitService.RetrofitUtil
import kotlinx.android.synthetic.main.custom_webview_fragment.*

class CustomWebviewFragment(var url: String) : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.custom_webview_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webviewInit()
        eventlistenerFun()
    }

    private fun webviewInit() {
        custom_webview.settings.javaScriptEnabled = true
        custom_webview.settings.allowContentAccess = true
        custom_webview.settings.allowFileAccess= true
        custom_webview.settings.setAppCacheEnabled(true)


        custom_webview.webViewClient = object : WebViewClient(){



            override fun onPageFinished(view: WebView?, url: String?) {
                Log.e("onPageFinished", url)
                super.onPageFinished(view, url)

                //success callback 일 경우만, 쿼피파라미터로 추출
                if(url!!.startsWith("http://jupjupsuccess.co.kr/?")){
                    var queryParam = Uri.parse(url)
                    Log.e("code==>",queryParam.getQueryParameter("code"))
                    var saveCodeData = queryParam.getQueryParameter("code")
                    // 성공하면, 종료하고 다신 안띄워야함
                    SharedUtil(activity!!).setSharedData("code",saveCodeData)
                    // token 받기
                    activity!!.onBackPressed()
                }

            }

            // deprecated 될 예정
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                Log.e("UrlLoading", url)
                return super.shouldOverrideUrlLoading(view, url)
            }

            // 변경해야함
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                Log.e("UrlLoading1", request!!.url.toString())
                return super.shouldOverrideUrlLoading(view, request)
            }


        }

        custom_webview.loadUrl(url)
    }


    // fragment 내 웹뷰 백버튼이벤트는 키 이벤트 리스너로 잡는다.
    private fun eventlistenerFun() {
        custom_webview.setOnKeyListener(object :View.OnKeyListener{
            override fun onKey(view: View?, keyCode: Int, event: KeyEvent?): Boolean {
                Log.e("keycode", keyCode.toString())
                Log.e("action", event!!.action.toString())

                if(event!!.action!=KeyEvent.ACTION_DOWN){
                    return true
                }

                if(keyCode == KeyEvent.KEYCODE_BACK){
                    Log.e("keycode111", custom_webview.canGoBack().toString())

                    if(custom_webview.canGoBack()){
                        custom_webview.goBack()
                    }else{
                        activity!!.onBackPressed()
                    }
                    return true
                }

                return false
            }
        })
    }


}
