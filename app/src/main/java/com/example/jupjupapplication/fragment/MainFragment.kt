package com.example.jupjupapplication.fragment

import android.animation.Animator
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.cleveroad.fanlayoutmanager.FanLayoutManager
import com.cleveroad.fanlayoutmanager.FanLayoutManagerSettings
import com.example.jupjupapplication.R
import com.example.jupjupapplication.Util.SharedUtil
import com.example.jupjupapplication.adapter.CardAdapter
import com.example.jupjupapplication.data.CardItem
import com.example.jupjupapplication.retrofitService.RetrofitUtil
import com.example.jupjupapplication.retrofitService.headerMap
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.leinardi.android.speeddial.SpeedDialView
import kotlinx.android.synthetic.main.fragment_layout.*
import java.io.ByteArrayOutputStream

class MainFragment :Fragment(){
    var list = ArrayList<CardItem>()
    lateinit var mFanLayoutManager : FanLayoutManager
    lateinit var speedDialView : SpeedDialView
    lateinit var retrofitUtil : RetrofitUtil
    var permission_call = android.Manifest.permission.CALL_PHONE
    var permission_cam = android.Manifest.permission.CAMERA

    var MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1
    var MY_PERMISSIONS_REQUEST_CAMERA = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        httpSetting()

        initCardData()

        val fanLayoutManagerSettings = FanLayoutManagerSettings
            .newBuilder(activity!!)
            .withFanRadius(true)
            .withViewHeightDp(350f)
            .withViewWidthDp(200f)
            .build()

        mFanLayoutManager = object: FanLayoutManager(activity!!,fanLayoutManagerSettings){}

        recyclerCard.layoutManager = mFanLayoutManager
        recyclerCard.adapter = CardAdapter(list)

        eventListener(mFanLayoutManager)

        floatingButton()

        setOauthInit()


    }

    private fun setOauthInit() {
        Log.e("getCode() ===>",getCode())
        Log.e("getToken() ===>",getToken())
        Log.e("getrefreshToken() ===>",getRefreshToken())
        Log.e("userSeq() ===>",getUserSeq())

        if(getCode().isNullOrBlank()){
            Log.e("getCode() ===>",getCode())
            goMakeUser()
        }else if(getToken().isNullOrBlank()){
            Log.e("getToken() ===>",getToken())
            retrofitUtil.getToken(getCode())
        }else{
            // 이제 토큰값 있으니까 이걸로 헤더에 박음
            headerMap.set("Authorization","Bearer "+getToken())
        }
    }

    private fun getUserSeq(): String {
        return SharedUtil(activity!!).getSharedData()!!.getString("user_seq_no","")!!
    }

    private fun getRefreshToken(): String {
        return SharedUtil(activity!!).getSharedData()!!.getString("refresh_token","")!!
    }

    private fun getToken(): String {
        return SharedUtil(activity!!).getSharedData()!!.getString("access_token","")!!
    }

    fun getCode(): String {
       return SharedUtil(activity!!).getSharedData()!!.getString("code","")!!
    }


    private fun goMakeUser() {
        goCumstomWebview(RetrofitUtil(activity).BASEURL_TEST+
                "/oauth/2.0/authorize?" +
                "response_type=code&"+
                "client_id=l7xx38e925b378c14a58a34a5880f5b2f306&"+
                "redirect_uri=http://jupjupSuccess.co.kr&"+
                "scope=login inquiry transfer&"+
                "client_info =test123&"+
                "auth_type=0&"+
                "lang=kor&"+
                "edit_option=on&"+
                "bg_color=#000000&"+
                "txt_color=#000000&"+
                "btn1_color=#000000&"+
                "btn2_color=#000000&"
        )
    }

    private fun httpSetting() {
        retrofitUtil = RetrofitUtil(activity)
        retrofitUtil.initOkHttpClient()
        retrofitUtil.initRetrofit()
        retrofitUtil.initHeaderSet()

//        retrofitUtil.setUser()
//        retrofitUtil.getToken()
//        retrofitUtil.getAuthoriczeCode()

//        retrofitUtil.getRefreshToken()
//        retrofitUtil.getPaylists()

//        retrofitUtil.getExtarPaylist()


        // 계좌실명조회
//        retrofitUtil.getRealUser()

        // 참가은행상태조회
        // retrofitUtil.getBankStatus()

    }




    private fun floatingButton() {
        speedDialView = speedDial

        speedDialView.addActionItem(
            SpeedDialActionItem.Builder(R.id.star, R.drawable.star)
                .setLabel("혜택")
                //아이콘 배경
                .setFabBackgroundColor(Color.WHITE)
                .create()
        )

        speedDialView.addActionItem(
            SpeedDialActionItem.Builder(R.id.phone, R.drawable.phone)
                .setLabel("고객센터")
                .setFabBackgroundColor(Color.WHITE)
                .create()
        )

        speedDialView.addActionItem(
            SpeedDialActionItem.Builder(R.id.card, R.drawable.card)
                .setLabel("사용내역")
                .setFabBackgroundColor(Color.WHITE)
                .create()
        )

        speedDialView.addActionItem(
            SpeedDialActionItem.Builder(R.id.testButton, R.drawable.card)
                .setLabel("테스트버튼")
                .setFabBackgroundColor(Color.WHITE)
                .create()
        )

        speedDialView.setOnActionSelectedListener(object : SpeedDialView.OnActionSelectedListener {
            override fun onActionSelected(actionItem: SpeedDialActionItem?): Boolean {

                var selected_item = (recyclerCard.adapter as CardAdapter).getItems()
                var selected_position = (recyclerCard.adapter as CardAdapter).getPosition()

                when(actionItem!!.id){
                    R.id.star -> goCumstomWebview("https://m.kbcard.com/CXHIACRC0002.cms?mainCC=b&allianceCode=09184")
                    R.id.phone -> goCumsomterCall()
                    R.id.card -> goUsePayFragment(selected_position,selected_item)
                    R.id.testButton -> retrofitUtil.getRegistAccount()
                }

                return false // true 면 계속 열려있기
            }
        })
    }

    private fun goCumsomterCall() {
        layoutClear()
        if(checkPermissionFun(permission_call)){
            //고객센터 전화걸기
            var telNumber = "01032275684"
            activity!!.startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:"+telNumber)))
        }else{
            reqeustPermissionFun(permission_call)
        }

    }

    // 퍼미션 요청
    private fun reqeustPermissionFun(permission:String) {
        when(permission){
            permission_call ->
                requestPermissions(
                    arrayOf(permission),
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS
                )
            permission_cam ->
                requestPermissions(
                    arrayOf(permission),
                    MY_PERMISSIONS_REQUEST_CAMERA
                )
        }
        // fragment는 이걸로 호출해야함

    }

    // 퍼미션 요청 응답
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        Log.e("requestCode", requestCode.toString())

        when(requestCode){
            MY_PERMISSIONS_REQUEST_READ_CONTACTS -> goCumsomterCall()
            MY_PERMISSIONS_REQUEST_CAMERA -> goCamaraCall()
        }
    }

    private fun checkPermissionFun(permission:String): Boolean {
        return ContextCompat.checkSelfPermission(activity!!.applicationContext,permission) == PackageManager.PERMISSION_GRANTED
    }

    private fun goCumstomWebview(setUrl :String) {
        layoutClear()
        //혜택 안내 사이트 열기
        var fragment = CustomWebviewFragment(setUrl)

        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.root,fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun layoutClear() {
        //화면전환시 초기화
        mFanLayoutManager.deselectItem()
    }

    private fun eventListener(mFanLayoutManager: FanLayoutManager) {
        // 리사이클 아이템 클릭
        (recyclerCard.adapter as CardAdapter).setOnItemClickListener(object : CardAdapter.OnItemClickListener{
            override fun onItemClicked(
                position: Int,
                view: View,
                items: ArrayList<CardItem>

            ) {
                if (mFanLayoutManager.selectedItemPosition != position) {
                    // 마지막 셀렉은 카메라 촬영 및 카드 등록이 되야함
                    if(position == list.size-1){
                        // qr카메라
                        Log.e("카메라 키자!","카메라 키자!")
                        goCamaraCall()
                    }else{
                        // 선택한거 확대
                        // 카드 이름 보여주기
                        Log.e("items[position].title",items[position].title)
                        card_name.setText(items[position].title)
                        card_name.setTextColor(Color.BLACK)

                        mFanLayoutManager.switchItem(recyclerCard, position)
                        point_image.visibility = View.VISIBLE
                        speedDialView.visibility =View.VISIBLE
                    }

                } else {
                    //한번 선택 된이후 진행할
                    mFanLayoutManager.straightenSelectedItem(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animator: Animator) {

                        }

                        override fun onAnimationEnd(animator: Animator) {
                            goUsePayFragment(position,items)
                        }

                        override fun onAnimationCancel(animator: Animator) {

                        }

                        override fun onAnimationRepeat(animator: Animator) {

                        }
                    })
                }
            }
        })

        // 리사이클러뷰 스크롤 리스너
        recyclerCard.addOnScrollListener(object: RecyclerView.OnScrollListener(){

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //스크롤 되고 있는 동안은 초기화
//                card_name.setText("카드")
//                if(dx !=0){
//                point_image.visibility = View.INVISIBLE
//                speedDialView.visibility = View.INVISIBLE
//                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                Log.e("onScrollStateChanged", newState.toString())

                if(newState == 2){
                    //  돌아가는 중에는 안보기
                    card_name.setText("")
                    point_image.visibility = View.INVISIBLE
                    speedDialView.visibility = View.INVISIBLE
                }else if(newState == 0){
                    var selected_item = (recyclerCard.adapter as CardAdapter).getItems()
                    var selected_position = (recyclerCard.adapter as CardAdapter).getPosition()

                    if(selected_position == list.size-1){
                        Log.e("마지막 !!","마지막은 새카드 등록!")
                        card_name.setText("카드를 등록해주세요")
                        point_image.visibility = View.INVISIBLE
                        speedDialView.visibility = View.INVISIBLE
                        mFanLayoutManager.collapseViews()
                    }else{
                        //  돌아가고 보기
                        card_name.setText(selected_item[selected_position].title)
                        point_image.visibility = View.VISIBLE
                        speedDialView.visibility = View.VISIBLE
                    }
                }

            }
        })

        // 포인트 이미지 클릭
        point_image.setOnClickListener(object:View.OnClickListener{
            override fun onClick(p0: View?) {
                layoutClear()
                movePointFragment()
            }
        })
    }



    private fun initCardData() {
        // 레트로핏 이용 데이터 가져옴


        // 리사이클러.어뎁터 = 커스텀 어뎁터 만들어서 매핑
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_petconomy_card),
                "펫코노미 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.kb_theeasy_card),
                "The easy 카드"
            )
        )
        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )

        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.km_cheongchuntitanum_card),
                "Titanium 카드"
            )
        )

        list.add(
            CardItem(
                activity?.getDrawable(R.drawable.plusbutton),
                "카드를 추가해주세요!"
            )
        )
    }

    private fun goUsePayFragment(position :Int , items: ArrayList<CardItem>) {
        layoutClear()

        var fragment = UsemoneyFragment()

        var bundle:Bundle = Bundle()
        bundle.putString("title",items[position].title)

        var bitmap : Bitmap? = items[position].image!!.toBitmap()
        var byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream)
        bundle.putByteArray("byteArray", byteArrayOutputStream.toByteArray())

        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.root,fragment)
            .addToBackStack(null)
            .commit()

        fragment.arguments = bundle
    }

    private fun movePointFragment() {
        var fragment = PointFragment()

        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.root,fragment)
            .addToBackStack(null)
            .commit()

    }

    fun deselectIfSelected(): Any {
        if (mFanLayoutManager.isItemSelected()) {
            mFanLayoutManager.deselectItem()
            return true
        } else {
            return false
        }
    }


    private fun goCamaraCall() {
        layoutClear()
        if(checkPermissionFun(permission_cam)){
            //카메라 오픈
            activity!!.startActivity(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
        }else{
            reqeustPermissionFun(permission_cam)
        }
    }



}