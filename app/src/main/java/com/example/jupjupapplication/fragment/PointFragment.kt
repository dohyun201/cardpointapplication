package com.example.jupjupapplication.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jupjupapplication.R
import com.example.jupjupapplication.adapter.PointListAdapter
import com.example.jupjupapplication.data.PointItem
import kotlinx.android.synthetic.main.point_fragment_layout.*

class PointFragment : Fragment() {

    var list : ArrayList<PointItem> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        InitData()
        point_list.adapter = PointListAdapter(list)

    }

    private fun InitData() {
        list.add(PointItem("전월 이용 실적 40만원" , "0 / 400,000"))
        list.add(PointItem("Favorite 사용 1만원 이상" , "0 / 1"))
        list.add(PointItem("SK , GS 칼텍스 주유소 이용" , "200,000P"))
        list.add(PointItem("전월 이용 실적 40만원" , "0 / 400,000"))
        list.add(PointItem("Favorite 사용 1만원 이상" , "0 / 1"))
        list.add(PointItem("SK , GS 칼텍스 주유소 이용" , "200,000P"))
        list.add(PointItem("전월 이용 실적 40만원" , "0 / 400,000"))
        list.add(PointItem("Favorite 사용 1만원 이상" , "0 / 1"))
        list.add(PointItem("SK , GS 칼텍스 주유소 이용" , "200,000P"))
        list.add(PointItem("전월 이용 실적 40만원" , "0 / 400,000"))
        list.add(PointItem("Favorite 사용 1만원 이상" , "0 / 1"))
        list.add(PointItem("SK , GS 칼텍스 주유소 이용" , "200,000P"))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.point_fragment_layout,container,false)
    }
}
