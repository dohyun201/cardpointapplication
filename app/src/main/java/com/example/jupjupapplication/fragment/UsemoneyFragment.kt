package com.example.jupjupapplication.fragment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jupjupapplication.R
import com.example.jupjupapplication.adapter.PayListAdapter
import com.example.jupjupapplication.data.PayData
import kotlinx.android.synthetic.main.pay_fragment.*

class UsemoneyFragment : Fragment(){

    var pointList = ArrayList<PayData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pay_fragment,container,false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // useMoneyFragment에서 가져온 번들 사용
        // 카드 타이틀 , 이미지
        var bundle = arguments
        Log.e("title===>",bundle!!.getString("title"))

        var title = bundle!!.getString("title")
        var position = bundle!!.getInt("position")
        var itemsBytes = bundle!!.getByteArray("byteArray")

        // 뷰 세팅
        viewInit(title , changeToBitmapImage(itemsBytes), position)
        // 이용 내역 리사이클러뷰
        initRecyclerListView()
    }

    private fun changeToBitmapImage(itemsBytes: ByteArray?): Bitmap {
        var bitmapImage : Bitmap = BitmapFactory.decodeByteArray(
            itemsBytes,
            0,
            itemsBytes!!.size
        )

        return bitmapImage
    }


    private fun viewInit(
        str: String?,
        items: Bitmap,
        position: Int
    ) {
        //card_name.setText(str)
        var drawable : Drawable = BitmapDrawable(items)
        card_selected_image.setImageDrawable(drawable)
    }

    private fun initRecyclerListView() {
        pointList.add(PayData("cu 이용", "200P / 30,000원"))
        pointList.add(PayData("차홍 미용실", "300P / 45,000원"))
        pointList.add(PayData("관리비", "10,000P / 150,000원"))
        pointList.add(PayData("cu 이용", "200P / 30,000원"))
        pointList.add(PayData("차홍 미용실", "300P / 45,000원"))
        pointList.add(PayData("관리비", "10,000P / 150,000원"))
        pointList.add(PayData("cu 이용", "200P / 30,000원"))
        pointList.add(PayData("차홍 미용실", "300P / 45,000원"))
        pointList.add(PayData("관리비", "10,000P / 150,000원"))
        pointList.add(PayData("cu 이용", "200P / 30,000원"))
        pointList.add(PayData("차홍 미용실", "300P / 45,000원"))
        pointList.add(PayData("관리비", "10,000P / 150,000원"))


        pay_list.adapter = PayListAdapter(pointList)
    }

}
