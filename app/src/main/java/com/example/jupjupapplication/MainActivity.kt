package com.example.jupjupapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.example.jupjupapplication.fragment.CustomWebviewFragment
import com.example.jupjupapplication.fragment.MainFragment
import com.example.jupjupapplication.retrofitService.RetrofitUtil
import kotlinx.android.synthetic.main.custom_webview_fragment.*

class MainActivity : AppCompatActivity() {

    lateinit var mMainFragment: MainFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState==null){

            var args = Bundle()
            mMainFragment = MainFragment()
            mMainFragment.arguments = args

            supportFragmentManager
                .beginTransaction()
                .add(R.id.root,mMainFragment)
                .commit()
        }else{
            var fragment : Fragment? = supportFragmentManager.findFragmentById(R.id.root)
            mMainFragment = fragment as MainFragment
        }

    }


    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.root)
        if (fragment is MainFragment) {
            mMainFragment = fragment
        }

        if (mMainFragment == null || !mMainFragment.isAdded || mMainFragment.deselectIfSelected() as Boolean) {
            super.onBackPressed()
            // 종료 팝업 필요
        }
    }

}
