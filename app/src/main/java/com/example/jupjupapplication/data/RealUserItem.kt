package com.example.jupjupapplication.data

data class RealUserItem(
    var api_tran_id : String,
    var api_tran_dtm : String,
    var rsp_code : String,
    var rsp_message : String,
    var bank_tran_id : String,
    var bank_tran_date : String,
    var bank_code_tran : String,
    var bank_rsp_code : String,
    var bank_rsp_message : String,
    var bank_code_std : String,
    var bank_code_sub : String,
    var bank_name : String,
    var account_num : String,
    var account_holder_info_type : String,
    var account_holder_info : String,
    var account_holder_name : String
    ) {

}
