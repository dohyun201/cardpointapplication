package com.example.jupjupapplication.data

data class UserItem  (var api_tran_id:String,
                 var api_tran_dtm:String,
                 var rsp_code:String,
                 var rsp_message:String,
                 var bank_tran_id:String,
                 var bank_tran_date:String,
                 var bank_code_tran:String,
                 var bank_rsp_code:String,
                 var bank_rsp_message:String,
                 var account_num : String,
                 var balance_amt:String,
                 var page_record_cnt:String,
                 var next_page_yn :String,
                 var befor_inquiry_trace_info : String,
                 var res_list:Object){

}
