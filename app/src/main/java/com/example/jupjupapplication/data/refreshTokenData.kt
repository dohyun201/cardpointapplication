package com.example.jupjupapplication.data

data class refreshTokenData (val access_token:String,
                        val token_type:String,
                        val expires_in:String,
                        val refresh_token:String,
                        val scope:String,
                        val user_seq_no:String
){

}

/*
* access_token <access_token> 오픈뱅킹에서 발행된 Access Token
token_type 고정값: Bearer Access Token 유형
expires_in <expire_time> Access Token 만료 기간(초)
refresh_token <refresh_token> Access Token 갱신 시 사용하는
Refresh Token
scope [login|inquiry|transfer] Access Token 권한 범위
user_seq_no “1000000106” AN(10) 사용자일련번호
*
* */
