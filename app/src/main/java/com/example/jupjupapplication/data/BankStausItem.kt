package com.example.jupjupapplication.data

data class BankStausItem(var api_tran_id:String,
                         var api_tran_dtm:String,
                         var rsp_code:String,
                         var rsp_message:String,
                         var res_cnt:String,
                         var res_list:Object,
                         var bank_code_std:String,
                         var bank_name:String,
                         var bank_status:String)


/*
* api_tran_id
"AA12349BHZ1324K82AL3"
Y
ANS(40)
거래고유번호(API)
api_tran_dtm
"20190910101921567"
Y
N(17)
거래일시(밀리세컨드)
rsp_code
"A0000"
Y
AN(5)
응답코드(API)
rsp_message
""
Y
AH(100)
응답메시지(API)
res_cnt
"16"
Y
N(5)
참가은행개수
res_list
<object>
Y
참가은행목록
bank_code_std
"097"
Y
AN(3)
참가은행.표준코드
bank_name
"오픈은행"
Y
AH(20)
참가은행명
bank_status
"Y"
Y
AN(1)
참가은행상태
Y:거래가능, D:장애 L:개시이전, F:종료예고상태 A:집계상태, E:종료
*
*
*
* */
