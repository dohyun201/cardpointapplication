package com.example.jupjupapplication.data

data class accessTokenData(val access_token:String,
                      val token_type:String,
                      val expires_in:String,
                      val refresh_token:String,
                      val scope:String,
                      val user_seq_no:String
                      ){

    /*
access_token <access_token> 오픈뱅킹에서 발행된 Access Token
token_type 고정값: Bearer Access Token 유형
expires_in <expire_time> Access Token 만료 기간(초)
scope 고정값: oob Access Token 권한 범위
client_use_code “F001234560” AN(10) 이용기관코드
* */

}
