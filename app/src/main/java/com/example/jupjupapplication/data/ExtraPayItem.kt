package com.example.jupjupapplication.data

data class ExtraPayItem (var api_tran_id:String
                         ,var api_tran_dtm:String
                         ,var rsp_code:String
                         ,var rsp_message:String
                         ,var bank_tran_id:String
                         ,var bank_tran_date:String
){

}


/*
* pi_tran_id
"AA12349BHZ1324K82AL3"
ANS(40)
거래고유번호(API)
api_tran_dtm
"20190910101921567"
N(17)
거래일시(밀리세컨드)
rsp_code
"A0000"
AN(5)
응답코드(API)
rsp_message
""
AH(100)
응답메시지(API)
bank_tran_id
"12345678901234567890"
AN(20)
거래고유번호(참가은행)
bank_tran_date
"20190910"
N(8)
거래일자(참가은행)
* */
