package com.example.jupjupapplication.data

data class AuthorizeItem(var code :String,var scope :String,var client_info :String) {

}


/*
*
* 값
code
<authorization_code>
사용자인증 성공 시 반환되는 코드
scope
[login|inquiry|transfer]
Access Token 권한 범위 (다중 scope 가능)
client_info
<client_info>
요청 시 이용기관이 셋팅한 정보를 그대로 전달

 */
