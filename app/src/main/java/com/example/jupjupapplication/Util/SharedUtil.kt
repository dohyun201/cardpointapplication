package com.example.jupjupapplication.Util

import android.app.Activity
import android.content.SharedPreferences
import androidx.core.app.ActivityCompat

class SharedUtil(var activity : Activity) {
    var pref = activity!!.getPreferences(0)
    var editor = pref.edit()

    fun setSharedData(key:String, data:String?) {
       editor.putString(key, data)
       editor.commit()
    }

  fun getSharedData(): SharedPreferences? {
      return pref
  }
}