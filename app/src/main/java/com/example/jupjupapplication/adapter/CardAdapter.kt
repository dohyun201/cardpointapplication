package com.example.jupjupapplication.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jupjupapplication.viewHolder.CardViewHolder
import com.example.jupjupapplication.R
import com.example.jupjupapplication.data.CardItem
import kotlinx.android.synthetic.main.card_layout.view.*

lateinit var mOnItemClickListener : CardAdapter.OnItemClickListener

class CardAdapter (private val items:ArrayList<CardItem>) : RecyclerView.Adapter<CardViewHolder>(){
    var seletedPosition = 0
    var seleteditems = ArrayList<CardItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val inflateView = LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false)
        return CardViewHolder(inflateView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        var cardImage = items[position].image
        holder.itemView.cardImage.setImageDrawable(cardImage)

        holder.itemView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                mOnItemClickListener.onItemClicked(holder.adapterPosition,holder.itemView,items)
            }
        })

        setPosition(holder.adapterPosition , items)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        mOnItemClickListener = onItemClickListener
    }


    interface OnItemClickListener {
        fun onItemClicked(
            position: Int,
            view: View,
            items: ArrayList<CardItem>
        )
    }


    fun setPosition(
        position: Int,
        items: ArrayList<CardItem>
    ){
        seletedPosition = position
        Log.e("setPosition", seletedPosition.toString())
        seleteditems = items

    }

    fun getPosition(): Int{
        return seletedPosition
    }

    fun getItems(): ArrayList<CardItem>{
        return seleteditems
    }
}