package com.example.jupjupapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jupjupapplication.R
import com.example.jupjupapplication.data.PointItem
import com.example.jupjupapplication.viewHolder.PointListHolder
import kotlinx.android.synthetic.main.point_list.view.*

class PointListAdapter(var item: ArrayList<PointItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflateView = LayoutInflater.from(parent.context).inflate(R.layout.point_list, parent, false)
        return PointListHolder(inflateView)
    }

    override fun getItemCount(): Int {
       return item.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var pointTitle = item[position].title
        var pointAmount = item[position].point
        holder.itemView.pointTitle.text = pointTitle
        holder.itemView.pointContent.text = pointAmount
    }

}
