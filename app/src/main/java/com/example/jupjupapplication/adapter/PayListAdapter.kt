package com.example.jupjupapplication.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jupjupapplication.viewHolder.PayListHolder
import com.example.jupjupapplication.R
import com.example.jupjupapplication.data.PayData
import kotlinx.android.synthetic.main.pay_list.view.*

class PayListAdapter(private val pointList: ArrayList<PayData>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflateView = LayoutInflater.from(parent.context).inflate(R.layout.pay_list, parent, false)
        return PayListHolder(inflateView)
    }


    override fun getItemCount(): Int {
        Log.e("pointList.size", pointList.size.toString())
        return pointList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var pointTitle = pointList[position].title
        var pointAmount = pointList[position].amount
        Log.e("pointAmount", pointAmount.toString())
        holder.itemView.useTitle.text = pointTitle
        holder.itemView.useMoney.text = pointAmount
    }

}
