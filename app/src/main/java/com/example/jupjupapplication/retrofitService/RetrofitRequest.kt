package com.example.jupjupapplication.retrofitService

import android.app.Activity
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.jupjupapplication.Util.SharedUtil
import com.example.jupjupapplication.data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


var headerMap : MutableMap<String,String> = mutableMapOf()

class RetrofitRequest(var context : Activity, var service : RetrofitService) {

    // 토큰 만료기간 90
    fun getAccessToken(code :String) {
        getHeader()
        Log.e("code===>",code)
        service.getAccessToken(
            code,
            "l7xx38e925b378c14a58a34a5880f5b2f306",
            "7f97a53da5914eb29ecd1c440acf38eb",
            "http://jupjupSuccess.co.kr",
            "authorization_code"
        ).enqueue(object : Callback<accessTokenData> {
            override fun onFailure(call: Call<accessTokenData>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                call: Call<accessTokenData>,
                response: Response<accessTokenData>
            ) {

                if(response.isSuccessful){
                    Log.e("getAccessToken body", response.body().toString())
                    headerMap.set("Authorization","Bearer "+response.body()!!.access_token)
                    // token값 저장
                    SharedUtil(context).setSharedData("access_token",response.body()!!.access_token)
                    SharedUtil(context).setSharedData("refresh_token",response.body()!!.refresh_token)
                    SharedUtil(context).setSharedData("user_seq_no",response.body()!!.user_seq_no)
                }
            }
        })
    }

    //거래 내역조회
    fun getPayList() {
        service.getPayList(
            "F123456789U4BC34239Z",
            "097",
            "1001234567890123",
            "1123456789",
            "A",
            "T",
            "20160404",
            "100000",
            "20160405",
            "110000",
            "D",
            "20190910101921",
            "123"
        ).enqueue(object : Callback<UserItem>{
            override fun onFailure(call: Call<UserItem>, t: Throwable) {
                Log.e("t",t.toString())
            }

            override fun onResponse(call: Call<UserItem>, response: Response<UserItem>) {
//                Log.e("response call", call.toString())
                Log.e("getPayList body", response.body().toString())
            }
        })
    }

    fun getPayLists() {
        setHeaderSecond()
        getHeader()

        service.getPayLists(
            "F123456789U4BC34239Z",
            "002",
            "1234567890123456",
            "1123456789",
            "A",
            "T",
            "20190101",
            "100000",
            "20190101",
            "110000",
            "D",
            "20190910101921",
            "123",
        headerMap
        ).enqueue(object : Callback<PayListData>{
            override fun onFailure(call: Call<PayListData>, t: Throwable) {
                Log.e("t",t.toString())
            }

            override fun onResponse(call: Call<PayListData>, response: Response<PayListData>) {
//                Log.e("response call", call.toString())
                Log.e("getPayList body", response.body().toString())
                if(isTokenExpired(response.body()!!.rsp_code))
                {
                    //토큰만료된거니까 리프레시
                    setHeader()
                    getRefreshToken(getSharedRefreshToken())
                }
            }
        })
    }

    private fun isTokenExpired(rspMessage: String): Boolean{
       return rspMessage.equals("O0003")
    }

    fun getSharedRefreshToken():String{
        return SharedUtil(context).getSharedData()!!.getString("refresh_token","")!!
    }

    fun getRefreshToken(refreshToken : String) {
        service.getrefreshToken(
            "l7xx38e925b378c14a58a34a5880f5b2f306",
            "7f97a53da5914eb29ecd1c440acf38eb",
            refreshToken,
            "login inquiry transfer",
            "refresh_token"
        ).enqueue(object : Callback<refreshTokenData> {
            override fun onFailure(call: Call<refreshTokenData>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                call: Call<refreshTokenData>,
                response: Response<refreshTokenData>
            ) {

                if(response.isSuccessful){
                    Log.e("getRefreshToken body", response.body().toString())
                    SharedUtil(context).setSharedData("access_token",response.body()!!.access_token)
                    SharedUtil(context).setSharedData("refresh_token",response.body()!!.refresh_token)
                }
            }
        })
    }

    //잔액조회
    fun getExtarPaylist() {
        getHeader()
        service.getExtraPaylist(
            "F123456789U4BC34239Z",
            "002",
            "1001234567890123",
            "1123456789",
            "20190910101921",
            headerMap
        ).enqueue(object : Callback<ExtraPayItem> {
            override fun onFailure(call: Call<ExtraPayItem>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                call: Call<ExtraPayItem>,
                response: Response<ExtraPayItem>
            ) {

                if(response.isSuccessful){
                    Log.e("getExtarPaylist body", response.body().toString())
                }
            }
        })
    }

    fun getAuthoriczeCode() {


        service.getAuthoriczeCode(
            "code",
            "l7xx38e925b378c14a58a34a5880f5b2f306",
            "http://jupjupSuccess.co.kr",
            "login",
            "1234",
            0,
            "kor",
            "on",
            "#000000",
            "#000000",
            "#000000",
            "#000000"
        ).enqueue(object : Callback<AuthorizeItem> {
            override fun onFailure(call: Call<AuthorizeItem>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                call: Call<AuthorizeItem>,
                response: Response<AuthorizeItem>
            ) {

                if(response.isSuccessful){
                    Log.e("getAuthoriczeCode body", response.body().toString())
                }
            }
        })
    }


    fun getBankStatus() {
        service.getBankStatus(
        ).enqueue(object : Callback<BankStausItem> {
            override fun onFailure(call: Call<BankStausItem>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                call: Call<BankStausItem>,
                response: Response<BankStausItem>
            ) {

                if(response.isSuccessful){
                    Log.e("getBankStatus res_cnt", response.body()!!.res_cnt)
                    Log.e("getBankStatus res_cnt", response.body()!!.res_list.toString())
//                    Log.e("getBankStatus body", response.body().toString())
                }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getRealUser() {
        var currentDate = LocalDateTime.now()
        var converTimeFormat = DateTimeFormatter.ofPattern("yyyymmddhhmmss")
        var convertDateTime = currentDate.format(converTimeFormat).toString()
        // 은행거리고유번호
//        ○ "하루 동안의 유일성 보장" 이외의 그 어떤 규칙(순차증가 등)도 보장하지 않습니다. ○ 형식 : 이용기관코드(10자리) + 생성주체구분코드(“U”)*+ 이용기관 부여번호(9자리
        service.getRealUser(
            "F123456789U4BC34239Z",
            "002",
            "1234567890123456",
            " ",
            "880101",
            convertDateTime
        ).enqueue(object : Callback<RealUserItem> {
            override fun onFailure(call: Call<RealUserItem>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                call: Call<RealUserItem>,
                response: Response<RealUserItem>
            ) {

                if(response.isSuccessful){
                    Log.e("getRealUser body", response.body().toString())
                }
            }
        })
    }

    fun setHeader() {
        headerMap.set("Content-Type","application/x-www-form-urlencoded; charset=UTF-8")
    }

    fun setHeaderSecond() {
        headerMap.set("Content-Type","application/json; charset=UTF-8")
    }

    fun getHeader(){
        Log.e("headerMap====>", headerMap.toString())
    }

    fun setUser() {
        service.setUser(
            "code",
            "l7xx38e925b378c14a58a34a5880f5b2f306",
            "http://jupjupSuccess.co.kr",
            "login inquiry transfer",
            "test123",
            "0",
            "kor",
            "on",
            "#000000",
            "#000000",
            "#000000",
            "#000000"
        ).enqueue(object : Callback<NewUserItem> {
            override fun onFailure(call: Call<NewUserItem>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                call: Call<NewUserItem>,
                response: Response<NewUserItem>
            ) {

                if(response.isSuccessful){
                    Log.e("setUser res_cnt", response.body()!!.toString())
//                    Log.e("getBankStatus body", response.body().toString())
                }
            }
        })
    }

    fun getRegistAccount() {
        var userSeq = getSequence()

        service.getRegistAccount(
                userSeq,
                "Y",
                "D",
                headerMap
        ).enqueue(object : Callback<AccountItem> {
            override fun onFailure(call: Call<AccountItem>, t: Throwable) {
                Log.e("t", t.toString())
            }

            override fun onResponse(
                    call: Call<AccountItem>,
                    response: Response<AccountItem>
            ) {

                if(response.isSuccessful){
                    Log.e("getRealUser body", response.body().toString())
                }
            }
        })
    }

    private fun getSequence(): String {
        return SharedUtil(context).getSharedData()!!.getString("user_seq_no","")!!
    }

}