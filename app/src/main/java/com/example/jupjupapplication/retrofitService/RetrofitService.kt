package com.example.jupjupapplication.retrofitService

import com.example.jupjupapplication.data.*
import retrofit2.Call
import retrofit2.http.*

interface RetrofitService {

//    api key : l7xx38e925b378c14a58a34a5880f5b2f306
//    api secret : 7f97a53da5914eb29ecd1c440acf38eb
//    @Headers(
//        "Content-type:application/json; charset=UTF-8",
//        "Authorization:Bearer 8080e06d-c50e-44e2-babe-75499baf1146"
//    )

    @GET("/v2.0/account/transaction_list/acnt_num")
    fun getPayList(
                   @Query("bank_tran_id") bank_tran_id:String,
                   @Query("bank_code_std") bank_code_std:String,
                   @Query("account_num") account_num:String,
                   @Query("user_seq_no") user_seq_no:String,
                   @Query("inquiry_type") inquiry_type:String,
                   @Query("inquiry_base") inquiry_base:String,
                   @Query("from_date") from_date:String,
                   @Query("from_time") from_time:String,
                   @Query("to_date") to_date:String,
                   @Query("to_time") to_time:String,
                   @Query("sort_order") sort_order:String,
                   @Query("tran_dtime") tran_dtime:String,
                   @Query("befor_inquiry_trace_info") befor_inquiry_trace_info:String
    ) : Call<UserItem>

    /*핀테크이용번호
    * 조회구분코드
      A:All,I:입금, O:출금
    * 조회시작일자
    * 조회종료일자
    * 정렬순서
      D:Descending,A:Ascending
    * 페이지번호
    * 요청일시
    * */

    //계좌번호 이용한 거리내역조회
//    @Headers(
//        "Content-type:application/json; charset=UTF-8",
//        "Authorization:Bearer 6415d16e-3237-4858-a79d-b068998000f2"
//    )
    //https://openapi.openbanking.or.kr/v2.0/account/transaction_list/acnt_num
    @POST("/v2.0/account/transaction_list/acnt_num")
    @FormUrlEncoded
    fun getPayLists(
        @Field("bank_tran_id") bank_tran_id:String,
        @Field("bank_code_std") bank_code_std:String,
        @Field("account_num") account_num:String,
        @Field("user_seq_no") user_seq_no:String,
        @Field("inquiry_type") inquiry_type:String,
        @Field("inquiry_base") page_index:String,
        @Field("from_date") from_date:String,
        @Field("from_time") from_time:String,
        @Field("to_date") to_date:String,
        @Field("to_time") to_time:String,
        @Field("sort_order") sort_order:String,
        @Field("tran_dtime") tran_dtime:String,
        @Field("befor_inquiry_trace_info") befor_inquiry_trace_info:String,
        @HeaderMap map : Map<String,String>
    ) : Call<PayListData>

    /*
bank_tran_id "F123456789U4BC34239Z" Y AN(20) 은행거래고유번호주 1)
bank_code_std "097" Y AN(3) 개설기관.표준코드
account_num "0001234567890123" Y AN(16) 계좌번호
user_seq_no "1123456789" Y AN(10) 사용자일련번호
inquiry_type "A" Y A(1) 조회구분코드
A:All, I:입금, O:출금
inquiry_base “D” Y A(1) 조회기준코드주 2)
D:일자, T:시간
from_date "20160404" Y N(8) 조회시작일자
from_time "100000" N N(6) 조회시작시간주 2)
to_date "20160405" Y N(8) 조회종료일자
to_time "110000" N N(6) 조회종료시간주 2)
sort_order "D" Y A(1) 정렬순서
D:Descending, A:Ascending
tran_dtime "20190910101921" Y N(14) 요청일시
befor_inquiry_trace_info "123" N AN(20) 직전조회추적정보
    * */


    //fintech_use_num:String, inquiry_type:String,from_date:String, to_date:String, sort_order:String, page_index:String , tran_dtime:String
    @POST("/oauth/2.0/token")
    @FormUrlEncoded
    fun getAccessToken(
                       @Field("code") code:String,
                       @Field("client_id") client_id:String,
                       @Field("client_secret") client_secret:String,
                       @Field("redirect_uri") redirect_uri:String,
                       @Field("grant_type") grant_type:String
    ) : Call<accessTokenData>

    /*
    * client_id : l7xx38e925b378c14a58a34a5880f5b2f306
    * client_secret : 7f97a53da5914eb29ecd1c440acf38eb
    * scope : oob 고정값
    * grant_type : client_credentials 고정
    * */
//    @Headers(
//        "Content-type:application/x-www-form-urlencoded",
//        "Authorization:Bearer 8080e06d-c50e-44e2-babe-75499baf1146"
//    )
    @POST("/oauth/2.0/token")
    @FormUrlEncoded
    fun getrefreshToken(@Field("client_id") clientId:String,
                       @Field("client_secret") clientSecret:String,
                       @Field("refresh_token") refresh_token:String,
                       @Field("scope") scope:String,
                       @Field("grant_type") grantType:String
    ) : Call<refreshTokenData>

//    @Headers(
//        "Content-type:application/x-www-form-urlencoded",
//        "Authorization:Bearer 8080e06d-c50e-44e2-babe-75499baf1146"
//    )

//    https://openapi.openbanking.or.kr/v2.0/account/balance/acnt_num
    @POST("/v2.0/account/balance/acnt_num")
    @FormUrlEncoded
    fun getExtraPaylist(
        @Field("bank_tran_id") bank_tran_id:String,
        @Field("bank_code_std") bank_code_std:String,
        @Field("account_num") account_num:String,
        @Field("user_seq_no") user_seq_no:String,
        @Field("tran_dtime") tran_dtime:String,
        @HeaderMap headerMap: MutableMap<String,String>
    ): Call<ExtraPayItem>


//    @Headers(
//        "Content-type:application/x-www-form-urlencoded",
//        "Authorization:Bearer 8080e06d-c50e-44e2-babe-75499baf1146"
//    )
    @GET("/oauth/2.0/authorize")
    fun getAuthoriczeCode(
        @Query("response_type") response_type:String,
        @Query("client_id") client_id:String,
        @Query("redirect_uri") redirect_uri:String,
        @Query("scope") scope:String,
        @Query("client_info") client_info:String,
        @Query("auth_type") auth_type:Int,
        @Query("lang") lang:String,
        @Query("edit_option") edit_option:String,
        @Query("bg_color") bg_color:String,
        @Query("txt_color") txt_color:String,
        @Query("btn1_color") btn1_color:String,
        @Query("btn2_color") btn2_color:String
    ): Call<AuthorizeItem>


//    @Headers(
//        "Content-type:application/x-www-form-urlencoded",
//        "Authorization:Bearer 8080e06d-c50e-44e2-babe-75499baf1146"
//    )
    @GET("/v2.0/bank/status")
    fun getBankStatus(
    ): Call<BankStausItem>


//    @Headers(
//        "Content-type:application/json; charset=UTF-8",
//        "Authorization:Bearer 8080e06d-c50e-44e2-babe-75499baf1146"
//    )
    @POST("/v2.0/inquiry/real_name")
    @FormUrlEncoded
    fun getRealUser(@Field("bank_tran_id") bank_tran_id: String,
                    @Field("bank_code_std") bank_code_std:String,
                    @Field("account_num") account_num:String,
                    @Field("account_holder_info _type") holder_info_type:String,
                    @Field("account_holder_info") account_holder_info:String,
                    @Field("tran_dtime") tran_dtime:String
    ) : Call<RealUserItem>

    @GET("/oauth/2.0/authorize")
    fun setUser(
        @Query("response_type") response_type:String,
        @Query("client_id") client_id:String,
        @Query("redirect_uri") redirect_uri:String,
        @Query("scope") scope:String,
        @Query("client_info") client_info:String,
        @Query("auth_type") auth_type:String,
        @Query("lang") lang:String,
        @Query("edit_option") edit_option:String,
        @Query("bg_color") bg_color:String,
        @Query("txt_color") txt_color:String,
        @Query("btn1_color") btn1_color:String,
        @Query("btn2_color") btn2_color:String
    ): Call<NewUserItem>


    //계좌 등록 확인
    @GET("/oauth/2.0/authorize_account")
    fun getRegistAccount(
            @Query("user_seq_no") user_seq_no:String,
            @Query("include_cancel_yn") include_cancel_yn:String,
            @Query("sort_order") sort_order:String,
            @HeaderMap map : Map<String,String>
    ): Call<AccountItem>

}
