package com.example.jupjupapplication.retrofitService

import android.app.Activity
import androidx.fragment.app.FragmentActivity
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitUtil(var activity: Activity?) {

    lateinit var service: RetrofitService
    lateinit var retrofit:Retrofit
    var BASEURL = "https://openapi.open-platform.or.kr" // 운영
    var BASEURL_TEST ="https://testapi.open-platform.or.kr"

    fun initOkHttpClient(): OkHttpClient? {
        //http 로그 보기 위함
            var builder: OkHttpClient.Builder = OkHttpClient.Builder()
            var interceptor : HttpLoggingInterceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            builder.addInterceptor(interceptor)
            return builder.build()
    }

    fun initRetrofit() {
        retrofit = Retrofit.Builder().baseUrl(BASEURL_TEST)
            //gson으로 파싱
            .addConverterFactory(GsonConverterFactory.create())
            //okhttp 로깅
            .client(RetrofitUtil(activity).initOkHttpClient())
            .build()
        service = retrofit.create(RetrofitService::class.java)
    }

    fun initHeaderSet() {
        RetrofitRequest(activity!!,service).setHeader()
    }


    fun getToken(code :String){
        RetrofitRequest(activity!!,service).getAccessToken(code)
    }

    fun getInstance(): Class<RetrofitUtil> {
        return RetrofitUtil::class.java
    }

    //거래내역 조회
    fun getPaylist() {
        RetrofitRequest(activity!!,service).getPayList()
    }


    //잔액조회
    fun getExtarPaylist() {
        RetrofitRequest(activity!!,service).getExtarPaylist()
    }

    // 3leg 인증
    fun getAuthoriczeCode() {
        RetrofitRequest(activity!!,service).getAuthoriczeCode()
    }

    fun getBankStatus() {
        RetrofitRequest(activity!!,service).getBankStatus()
    }

    fun getRealUser() {
        RetrofitRequest(activity!!,service).getRealUser()
    }

    fun getPaylists() {
        RetrofitRequest(activity!!,service).getPayLists()
    }

    fun setUser() {
        RetrofitRequest(activity!!,service).setUser()
    }

    fun getRegistAccount() {
        RetrofitRequest(activity!!,service).getRegistAccount()
    }


}
